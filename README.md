# Giphy Search

Search some cool gifs in real time 🦦

## Usage

#### Set up the `API key`:

1. get an API key from giphy - [instructions](https://developers.giphy.com/docs/api/#quick-start-guide)
2. create a `.env` file in the root folder
3. add the following to the file: `REACT_APP_GIPHY_API_KEY=your_api_key`

#### Run the app:
```
npm install
npm start
```

Visit [http://localhost:3000](http://localhost:3000) to see the app in the browser.

#### Run the tests:
```
npm test
```

## Tools
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).