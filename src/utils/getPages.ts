export const getPages = (totalItems: number, itemsPerPage: number) => {
  let pages = [];
  for (let i = 1; i <= Math.ceil(totalItems / itemsPerPage); i++) {
    pages.push(i);
  }
  return pages;
};
