import { describe, expect, it } from 'vitest';
import { getPages } from './getPages';

describe('getPages', () => {
  it('should return number of pages', () => {
    const totalItems = 10;
    const itemsPerPage = 2;
    const expectedOutput = [1, 2, 3, 4, 5];

    const output = getPages(totalItems, itemsPerPage);

    expect(output).toEqual(expectedOutput);
  });

  it('should return extra page for the odd item', () => {
    const totalItems = 10;
    const itemsPerPage = 3;
    const expectedOutput = [1, 2, 3, 4];

    const output = getPages(totalItems, itemsPerPage);

    expect(output).toEqual(expectedOutput);
  });

  it('should return empty array if total items is 0', () => {
    const totalItems = 0;
    const itemsPerPage = 3;
    const expectedOutput = [] as number[];

    const output = getPages(totalItems, itemsPerPage);

    expect(output).toEqual(expectedOutput);
  });
});
