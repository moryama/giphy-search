import { useEffect, useState } from 'react';
import { IGif } from '@giphy/js-types';
import { searchByText } from '../services/searchByText';

const DELAY_IN_MS = 300;

export const useSearchGifByText = () => {
  const [searchText, setSearchText] = useState('');
  const [gifs, setGifs] = useState<IGif[]>([]);

  useEffect(() => {
    const fetchGifs = async () => {
      const response = await searchByText(searchText);
      setGifs(response);
    };

    const fetchWithDelay = setTimeout(() => {
      fetchGifs();
    }, DELAY_IN_MS);

    return () => clearTimeout(fetchWithDelay);
  }, [searchText]);

  return { gifs, searchText, setSearchText };
};
