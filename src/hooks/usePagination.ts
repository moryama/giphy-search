import { useState } from 'react';
import { IGif } from '@giphy/js-types';

export const GIFS_PER_PAGE = 6;

export const usePagination = (gifs: IGif[]) => {
  const [currentPage, setCurrentPage] = useState(1);
  const lastGifIndex = currentPage * GIFS_PER_PAGE;
  const firstGifIndex = lastGifIndex - GIFS_PER_PAGE;

  const currentGifsToShow = gifs.slice(firstGifIndex, lastGifIndex);

  return { currentGifsToShow, currentPage, setCurrentPage };
};
