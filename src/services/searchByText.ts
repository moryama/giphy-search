import axios from 'axios';
import { IGif } from '@giphy/js-types';

const BASE_URL = 'https://api.giphy.com/v1/gifs/search?';
const LIMIT = 50;

export const searchByText = async (text: string): Promise<IGif[]> => {
  const searchUrl = `${BASE_URL}q=${text}&api_key=${process.env.REACT_APP_GIPHY_API_KEY}&limit=${LIMIT}`;
  const response = await axios.get(searchUrl);
  return response.data.data;
};
