import { GifList } from './GifList';
import { Pagination } from './Pagination';
import { usePagination } from '../hooks/usePagination';
import { IGif } from '@giphy/js-types';

type GifsViewProps = {
  gifs: IGif[];
};

export const GifsView = ({ gifs }: GifsViewProps) => {
  const { currentGifsToShow, currentPage, setCurrentPage } =
    usePagination(gifs);

  return (
    <>
      <Pagination
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        totalGifs={gifs.length}
      />
      {currentGifsToShow.length > 0 && <GifList gifs={currentGifsToShow} />}
    </>
  );
};
