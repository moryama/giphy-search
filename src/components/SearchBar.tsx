import * as S from './SearchBar.styles';

type SearchBarProps = {
  text: string;
  setText: React.Dispatch<React.SetStateAction<string>>;
};

export const SearchBar = ({ text, setText }: SearchBarProps) => {
  return (
    <S.SearchBarWrapper>
      <S.InputField
        autoFocus
        type="text"
        placeholder="search gifs"
        value={text}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
          setText(e.target.value)
        }
      ></S.InputField>
    </S.SearchBarWrapper>
  );
};
