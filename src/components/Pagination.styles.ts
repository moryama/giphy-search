import styled from 'styled-components';

type PaginationButtonProps = {
  readonly isSelected: boolean | undefined;
};

export const PaginationButton = styled.button<PaginationButtonProps>`
  font-size: large;

  background-color: ${(props) => (props.isSelected ? '#FF6D60' : '#F3E99F')};
  color: ${(props) => (props.isSelected ? 'white' : '')};

  border: solid grey 1px;
  padding: 10px 20px 10px 20px;
`;

export const PaginationWrapper = styled.div`
  margin-bottom: 50px;
`;
