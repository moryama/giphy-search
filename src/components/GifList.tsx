import { IGif } from '@giphy/js-types';
import { GifItem } from './GifItem';
import * as S from './GifList.styles';

type GifListProps = {
  gifs: IGif[];
};

export const GifList = ({ gifs }: GifListProps) => {
  return (
    <S.GifListWrapper>
      {gifs.map((gif) => (
        <GifItem gif={gif} />
      ))}
    </S.GifListWrapper>
  );
};
