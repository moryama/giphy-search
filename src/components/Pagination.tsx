import { GIFS_PER_PAGE } from '../hooks/usePagination';
import { getPages } from '../utils/getPages';
import * as S from './Pagination.styles';

type PaginationProps = {
  currentPage: number;
  setCurrentPage: React.Dispatch<React.SetStateAction<number>>;
  totalGifs: number;
};

export const Pagination = ({
  currentPage,
  setCurrentPage,
  totalGifs,
}: PaginationProps) => {
  const pages = getPages(totalGifs, GIFS_PER_PAGE);

  return (
    <S.PaginationWrapper>
      {pages.map((page) => (
        <S.PaginationButton
          isSelected={page === currentPage}
          key={page}
          onClick={() => setCurrentPage(page)}
        >
          {page}
        </S.PaginationButton>
      ))}
    </S.PaginationWrapper>
  );
};
