import styled from 'styled-components';

export const SearchBarWrapper = styled.div`
  margin-bottom: 50px;
`;

export const InputField = styled.input`
  font-size: x-large;
  border-radius: 5px;
  padding: 10px 15px 10px 15px;
  border: solid grey 1.5px;
`;
