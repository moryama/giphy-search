import { IGif } from '@giphy/js-types';
import * as S from './GifItem.styles';

type GifItemProps = {
  gif: IGif;
};

export const GifItem = ({ gif }: GifItemProps) => {
  return (
    <S.GifItem key={gif.id}>
      <img src={gif.images.fixed_height.url} alt={gif.title} />
    </S.GifItem>
  );
};
