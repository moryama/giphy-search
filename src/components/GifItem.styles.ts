import styled from 'styled-components';

export const GifItem = styled.div`
  border-radius: 20px;
  padding: 1rem 2rem;

  img {
    border-radius: 10px;
  }
`;
