import { useSearchGifByText } from './hooks/useSearchGifByText';
import * as S from './App.styles';
import { SearchBar } from './components/SearchBar';
import { GifsView } from './components/GifsView';

function App() {
  const { gifs, searchText, setSearchText } = useSearchGifByText();

  return (
    <S.AppWrapper>
      <SearchBar text={searchText} setText={setSearchText} />
      <GifsView gifs={gifs} />
    </S.AppWrapper>
  );
}

export default App;
